#!/bin/bash

function do_cmd {
	if [ "${TEST}" == "no" ]
	then
		$*
	else
		echo '     ' $*
	fi
}

SYSCTL='/usr/sbin/sysctl'
IP='/usr/sbin/ip'
TC='/usr/sbin/tc'
IPTABLES='/usr/sbin/iptables'
BUSYBOX='/usr/bin/busybox'
IPERF='/usr/bin/iperf3'
DHRELAY='/usr/sbin/dhcrelay'
DHCPCD='/usr/sbin/dhcpcd'
BUSYBOX='/usr/bin/busybox'

FILE='/simconf.json'
TEST='no'
RELEASE='yes'
CMD=$0

while getopts 'knf:' OPTION
do
	case ${OPTION} in
		n)
			TEST='yes'
			;;
		k)
			RELEASE='no'
			;;
		f)
			FILE=${OPTARG}
			;;
	esac
done

shift $(( OPTIND - 1 ))

if [ ! -r "${FILE}" ]
then
	echo 'Bad configuration <'${FILE}'>'
	exit 1
fi

#= Flush Device =============================================================
function flush_device {
	local Q=${1}

	printf -v NS 'D%u' ${Q}

	if [ -f /run/udhcpc-F${Q}.pid ]
	then
		local P=`cat /run/udhcpc-F${Q}.pid`
		if [ -d /proc/${P} ]
		then
 			if [ "`readlink /proc/${P}/exe`" == "${BUSYBOX}" ]
			then
				if [ "${RELEASE}" == "yes" ] 
				then
					do_cmd ${IP} netns exec ${NS} kill -SIGTERM ${P}
				else
					do_cmd ${IP} netns exec ${NS} kill -SIGKILL ${P}
				fi
			fi
		fi
		do_cmd rm -f /run/udhcpc-F${Q}.pid
	fi

	do_cmd ${IP} -n ${NS} -4 address flush dev F${Q}
	do_cmd ${IP} -n ${NS} -4 route flush default
}

#= Connect Device ===========================================================
function connect_device {
	local Q
	local NAME

	local NS

	Q=$1
	NAME=$2

	printf -v NS 'D%u' ${Q}

	echo CONNECT ${NS} '('${NAME}')'

	flush_device ${Q}

	do_cmd ${IP} netns exec ${NS} ${BUSYBOX} udhcpc -i F${Q} -n -R -s /jump/udhcpc.sh -p /run/udhcpc-F${Q}.pid -x hostname:${NAME}
}

#= Release Device ===========================================================
function release_device {
	local Q

	local NS

	Q=$1

	printf -v NS 'D%u' ${Q}

	echo RELEASE ${NS}

	flush_device ${Q}
}

#= Device Move ==============================================================
function device_move {
	local Q=${1}
	local A=${2}

	flush_device ${Q}

	do_cmd ${IP} -4 link set E${Q} master B${A}
}

#= Device Static Configuration ==============================================
function device_static {
	local Q=${1}
	local A=${2}
	local G=${3}
	local D=${4:-"10.0.254.1"}

	flush_device ${Q}

	do_cmd ${IP} -n D${Q} -4 address add ${A} dev F${Q}
	do_cmd ${IP} -n D${Q} -4 route add default via ${G}
	cat > /etc/netns/D${Q}/resolv.conf << %%
# Static
nameserver ${D}
%%
}

#= Device Status ============================================================
function device_status {
	local Q=${1}
	local A=`${IP} -n D${Q} -4 -br -j addr show dev F${Q}`
	local L=`${IP} -4 -j link show dev E${Q}`
	local M=`${IP} -n D${Q} -4 -j link show dev F${Q}`

	local ADDRESS=`echo ${A} | jq -r '.. | .local? | select(. != null)'`
	local PREFIXLEN=`echo ${A} | jq -r '.. | .prefixlen? | select(. != null)'`
	local MASTER=`echo ${L} | jq -r '.. | .master? | select(. != null)'`
	# local FLAGS=`echo ${L} | jq -r '.. | .flags? | select(. != null) | .[]'`
	local STATE=`echo ${L} | jq -r '.. | .operstate? | select(. != null)'`
	local MAC=`echo ${M} | jq -r '.. | .address? | select(. != null)'`

	if [ "${ADDRESS}" != "" ]
	then
		echo '  '${Q}':' ${ADDRESS}/${PREFIXLEN} '('${MAC}')' ${STATE} '->' ${MASTER:1}
	else
		if [ -t 1 ]
		then
			echo -n -e '  '${Q}':\r'
		fi
	fi
}

#= Device List ==============================================================
function device_list {
	local Q=${1}
	local CONNECTED_TO
	local MAC=`${IP} -n D${Q} -4 -j link show dev F${Q} | jq -r '.. | .address? | select(. != null)'`

	DEV_NAME=`cat ${FILE} | jq -r -c '.device['${Q}'].name'`
	DEV_USE=`cat ${FILE} | jq -r -c '.device['${Q}'].use'`
	CONNECTED_TO=`${IP} -4 -j link show dev E${Q} | jq -r '.[].master?'`

	if [ "B${DEV_USE}" == "${CONNECTED_TO}" ]
	then
		echo '  '${Q}':' ${MAC} '['${DEV_NAME}']' '->' ${DEV_USE}
	else
		echo '  '${Q}':' ${MAC} '['${DEV_NAME}']' '->' ${DEV_USE} '(now '${CONNECTED_TO:1}')'
	fi
}

#= Device Session ===========================================================
function device_session {
	local Q=${1}

	do_cmd ${IP} netns exec D${Q} bash
}

#= Operations ===============================================================
function operation {
	local NAME

	case $1 in
		connect) 
			NAME=`cat ${FILE} | jq -r '.device['${2}'].name'`
			connect_device $2 ${NAME};;
		release) 
			release_device $2;;
		status) 
			device_status $2;;
		list) 
			device_list $2;;
		*)
			echo 'Unknown operation' $1;;
	esac
}

#===========================================================================#
#= Main Program ============================================================#
#===========================================================================#

# NOW=`date +%Y-%m-%d-%H-%M-%S`
# LOG='/root/setup-lan.log.'${NOW}

# set -x
# exec 1<&-
# exec 2<&-
# exec 1<>${LOG}
# exec 2>&1

if [ "${TEST}" == "no" -a "`id -u`" != "0" ]
then
	echo "You must be root."
	exit 2
fi

if [ "`${IP} netns identify`" != "" ]
then
	echo "Must be run from root namespace."
	exit 2
fi

N_DEV=`cat ${FILE} | jq -r -c '.device | length'`

if [ "$#" -eq "1" -a "$1" == "status" ]
then
	for (( n=0; n<${N_DEV}; n++ ))
	do
		operation 'status' $n
	done
elif [ "$#" -eq "1" -a "$1" == "list" ]
then
	for (( n=0; n<${N_DEV}; n++ ))
	do
		operation 'list' $n
	done
elif [ "$#" -eq "1" -a "$1" == "endpoints" ]
then
	for n in `cat ${FILE} | jq -r -c '.vlan[]'`
	do
		V_ID=`echo ${n} | jq -r '.id'`
		ROUTER=`echo ${n} | jq -r '.router? | select (. != null)'`
		if [ "${ROUTER}" == "" ]
		then
			echo '  '${V_ID}
		else
			for m in `echo ${ROUTER} | jq -c '.[]'`
			do
				for p in `echo ${m} | jq -c -r '.subnet[]'`
				do
					S_ID=`echo ${p} | jq -r '.id'`
					echo '  '$S_ID
				done
			done
		fi
	done
elif [ "$#" -eq "1" ]
then
	if [ $1 -lt ${N_DEV} ]
	then
		device_session $1
	else
		echo "No such device" $2
	fi
elif [ "$#" -ge "4" -a "$1" == "static" ]
then
	if [ $2 -lt ${N_DEV} ]
	then
		shift
		device_static $*
	else
		echo "No such device" $2
	fi
elif [ "$#" -eq "3" -a "$1" == "move" ]
then
	if [ $2 -lt ${N_DEV} ]
	then
		shift
		device_move $*
	else
		echo "No such device" $2
	fi
elif [ "$#" -ge "2" -a "$1" == "monitor" ]
then
	if [ $2 -lt ${N_DEV} ]
	then
		if [ "$#" -ge "3" ]
		then
			do_cmd ${IP} netns exec D${2} bwm-ng -I F${2} -T avg -A ${3} -u bits
		else
			do_cmd ${IP} netns exec D${2} bmon -p F${2} -b
		fi
	else
		echo "No such device" $2
	fi
elif [ "$#" -ge "3" -a "$1" == "capture" ]
then
	if [ $2 -lt ${N_DEV} ]
	then
		A=`${IP} -n D${2} -4 -br -j addr show dev F${2} | jq -r '.. | .local? | select(. != null)'`

		if [ "${A}" == "" ]
		then
			echo "Device" $2 "is not connected"
		else
			if [ "$#" == "3" ]
			then
				do_cmd ${IP} netns exec D${2} tshark -n -i F${2} -w ${3} ip host ${A}
			elif [ "$3" == "u" ]
			then
				do_cmd ${IP} netns exec D${2} tshark -n -i F${2} -w ${4} ip src host ${A}
			elif [ "$3" == "d" ]
			then
				do_cmd ${IP} netns exec D${2} tshark -n -i F${2} -w ${4} ip dst host ${A}
			else
				echo "" $2
			fi
		fi
	else
		echo "No such device" $2
	fi
elif [ "$#" -ge "2" ]
then
	OP=$1
	if [ "$2" == "all" ]
	then
		for (( n=0; n<${N_DEV}; n++ ))
		do
			operation ${OP} $n
		done
	else
		while [ "$#" -ge "2" ]
		do
			if [ $2 -lt ${N_DEV} ]
			then
				operation ${OP} $2
			else
				echo "No such device" $2
			fi

			shift
		done
	fi
else
	echo ${CMD} '[-f <config-file>] list|status [all]'
	echo ${CMD} '[-f <config-file>] list|status <device-number> [<device-number> [...]]'
	echo ${CMD} '[-f <config-file>] [-k] connect|release all'
	echo ${CMD} '[-f <config-file>] [-k] connect|release <device-number> [<device-number> [...]]'
	echo ${CMD} '[-f <config-file>] [-k] static <device-number> <ip> <gateway> [<dns>]'
	echo ${CMD} '[-f <config-file>] [-k] move <device-number> <use-endpoint>'
	echo ${CMD} '[-f <config-file>] monitor <device-number> [<average-over>]'
	echo ${CMD} '[-f <config-file>] capture <device-number> [u|d] <capture-file>'
	echo ${CMD} '[-f <config-file>] endpoints'
	echo ${CMD} '[-f <config-file>] <device-number>'
	exit 1
fi

