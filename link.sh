#!/bin/bash

function do_cmd {
	if [ "${TEST}" == "no" ]
	then
		$*
	else
		echo '     ' $*
	fi
}

SYSCTL='/usr/sbin/sysctl'
IP='/usr/sbin/ip'
TC='/usr/sbin/tc'
IPTABLES='/usr/sbin/iptables'
BUSYBOX='/usr/bin/busybox'
IPERF='/usr/bin/iperf3'
DHRELAY='/usr/sbin/dhcrelay'
DHCPCD='/usr/sbin/dhcpcd'
BWM='/usr/bin/bwm-ng'

FILE='/simconf.json'
TEST='no'
HARD='no'
CMD=$0

while getopts 'nHf:' OPTION
do
	case ${OPTION} in
		n)
			TEST='yes'
			;;
		H)
			HARD='yes'
			;;
		f)
			FILE=${OPTARG}
			;;
	esac
done

shift $(( OPTIND - 1 ))

if [ ! -r "${FILE}" ]
then
	echo 'Bad configuration <'${FILE}'>'
	exit 1
fi

#= Link Up ==================================================================
function link_up {
	local Q
	local NAME

	local NS

	Q=$1
	NAME=$2

	printf -v NS 'S%u' ${Q}

	echo UP ${NS} '(W'${Q}', '${NAME}')'

	if [ "${HARD}" == "no" ]
	then
		do_cmd ${IP} link set W${Q} up
	else
		do_cmd ${IP} -n ${NS} link set ${NAME} up
	fi
}

#= Link Down ================================================================
function link_down {
	local Q
	local NAME

	local NS

	Q=$1
	NAME=$2

	printf -v NS 'S%u' ${Q}

	echo DOWN ${NS} '(W'${Q}', '${NAME}')'

	if [ "${HARD}" == "no" ]
	then
		do_cmd ${IP} link set W${Q} down
	else
		do_cmd ${IP} -n ${NS} link set ${NAME} down
	fi
}


#= Link Status ==============================================================
function link_status {
	local Q
	local NAME

	local NS

	Q=$1
	NAME=$2

	printf -v NS 'S%u' ${Q}

	echo STATUS  ${NS} '(W'${Q}', '${NAME}')'
	echo -n '  '; do_cmd ${IP} -n S${Q} -br link show dev ${NAME}
	echo -n '  '; do_cmd ${IP} -br link show dev W${Q}
}

#= Link Configuration =======================================================
function link_netem {
	local Q
	local NAME

	local NS

	Q=$1
	NAME=$2

	printf -v NS 'S%u' ${Q}

	shift 2

	if [ "$#" -eq 0 ]
	then
		echo SPEED  ${NS} '(W'${Q}', '${NAME}')'
		echo -n '  D '; do_cmd ${TC} -n S${Q} qdisc show dev ${NAME}
		echo -n '  U '; do_cmd ${TC} -n S${Q} qdisc show dev U${Q}
	elif [ "$1" == "ul" ]
	then
		shift
		do_cmd ${TC} -n S${Q} qdisc change dev U${Q} root netem $*
	elif [ "$1" == "dl" ]
	then
		shift
		do_cmd ${TC} -n S${Q} qdisc change dev ${NAME} root netem $*
	else
		do_cmd ${TC} -n S${Q} qdisc change dev ${NAME} root netem $*
		do_cmd ${TC} -n S${Q} qdisc change dev U${Q} root netem $*
	fi
}

#= Operations ===============================================================
function operation {
	local NAME
	local CMD

	CMD=$3
	NAME=`cat ${FILE} | jq -r '.wan['${2}'].interface'`

	case $1 in
		up) 
			link_up $2 ${NAME};;
		down) 
			link_down $2 ${NAME};;
		*)
			usage ${CMD}
			exit 1
			;;
	esac
}

#= Operations ===============================================================
function usage {
	local CMD

	CMD=$1

	echo ${CMD} '[-f <config-file>] monitor [<average-over>]'
	echo ${CMD} '[-f <config-file>] status'
	echo ${CMD} '[-f <config-file>] netem'
	echo ${CMD} '[-f <config-file>] leases'
	echo ${CMD} '[-f <config-file>] netem <link-number> [ul|dl] <link parameters: rate, loss, delay, ...>'
	echo ${CMD} '[-f <config-file>] [-H] up|down all'
	echo ${CMD} '[-f <config-file>] [-H] up|down <link-number> [<link-number> [...]]'
}

#===========================================================================#
#= Main Program ============================================================#
#===========================================================================#

# NOW=`date +%Y-%m-%d-%H-%M-%S`
# LOG='/root/setup-lan.log.'${NOW}

# set -x
# exec 1<&-
# exec 2<&-
# exec 1<>${LOG}
# exec 2>&1

if [ "${TEST}" == "no" -a "`id -u`" != "0" ]
then
	echo "You must be root."
	exit 2
fi

if [ "`${IP} netns identify`" != "" ]
then
	echo "Must be run from root namespace."
	exit 2
fi

N_LINK=`cat ${FILE} | jq -r -c '.wan | length'`

if [ "$#" -ge "1" -a "$1" == "monitor" ]
then
	LINKS=''
	for (( n=0; n<${N_LINK}; n++ ))
	do
		LINKS=${LINKS}'W'${n}','
	done

	if [ "$#" -ge "2" ]
	then
		do_cmd ${BWM} -u bits -I ${LINKS} -T avg -A ${2}
	else
		do_cmd ${BWM} -u bits -I ${LINKS}
	fi
	exit 1
elif [ "$#" -eq "1" -a "$1" == "status" ]
then
	NAME=`cat ${FILE} | jq -r '.lan'`
	echo STATUS LAN '('${NAME}')'
	echo -n '  '; do_cmd ${IP} -br link show dev ${NAME}
	for (( n=0; n<${N_LINK}; n++ ))
	do
		NAME=`cat ${FILE} | jq -r '.wan['${n}'].interface'`
		link_status ${n} ${NAME}
	done
elif [ "$#" -eq "1" -a "$1" == "netem" ]
then
	for (( n=0; n<${N_LINK}; n++ ))
	do
		NAME=`cat ${FILE} | jq -r '.wan['${n}'].interface'`
		link_netem ${n} ${NAME}
	done
elif [ "$#" -gt "2" -a "$1" == "netem" ]
then
	NAME=`cat ${FILE} | jq -r '.wan['${2}'].interface'`
	LINK=$2
	shift 2
	link_netem ${LINK} ${NAME} $*
elif [ "$#" -eq "1" -a "$1" == "leases" ]
then
	for (( n=0; n<${N_LINK}; n++ ))
	do
		echo LEASES W${n}
		busybox dumpleases -f /tmp/lease.W${n} | tail -n +2
	done
elif [ "$#" -ge "2" ]
then
	OP=$1

	if [ "$2" == "all" ]
	then
		for (( n=0; n<${N_LINK}; n++ ))
		do
			operation ${OP} ${n} ${CMD}
		done
	else
		while [ "$#" -ge "2" ]
		do
			if [ $2 -lt ${N_LINK} ]
			then
				operation ${OP} $2 ${CMD}
			else
				echo "No such link" $2
			fi

			shift
		done
	fi
else
	usage ${CMD}
	exit 1
fi

