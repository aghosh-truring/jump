#!/bin/bash

IP='/usr/sbin/ip'
NOW=`date +%Y-%m-%d-%H-%M-%S`
NS=`${IP} netns identify`

if [ "${NS}" == "" ]
then
	## In root namespace! Bail out...
	echo "${NOW} udhcpc script is in root namespace!"
	exit 1
else
	RESOLVE="/etc/netns/${NS}/resolv.conf"
fi

case $1 in

	bound)
		${IP} -4 route flush default
		${IP} -4 address flush dev ${interface}

		${IP} -4 address add ${ip}/${subnet} dev ${interface}

		for n in ${router}
		do
			${IP} -4 route add default via ${n} 

			# Only the first router will be added
			break
		done

		echo '# Namespace' ${NS} 'At' ${NOW} > ${RESOLVE}
		for n in ${dns}
		do
			echo 'nameserver' ${n} >> ${RESOLVE}
		done
		;;

	renew)
		for n in ${router}
		do
			${IP} -4 route change default via ${n} 
			break
		done

		echo '# Namespace' ${NS} 'At' ${NOW} > ${RESOLVE}
		for n in ${dns}
		do
			echo 'nameserver' ${n} >> ${RESOLVE}
		done
		;;

	deconfig)
		${IP} -4 route flush default
		${IP} -4 address flush dev ${interface}
		;;

	*)	;;

esac


