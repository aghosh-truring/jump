#!/bin/bash

if [ ! -d /etc/tinc ]
then
	echo 'TINC not installed?'
	exit 2
elif [ -d /etc/tinc/testnet ]
then
	echo 'Already configured?'
	exit 2
elif [ "$#" -ne "2" ]
then
	echo "Usage: $0 name number"
	exit 1
elif (( $2 == 0 ))
then
	echo "Usage: $2 not number"
	exit 1
else
	TINC_NAME=${1}
	TINC_NUMBER=${2}
fi

TINC_IP='10.255.7.'${TINC_NUMBER}

echo ${TINC_NAME} '->' ${TINC_IP}

set -x 

mkdir -p /etc/tinc/testnet/hosts
cp /jump/tinc/tiger /etc/tinc/testnet/hosts/
cp /jump/tinc/lion /etc/tinc/testnet/hosts/

## Create host file
cat > /etc/tinc/testnet/hosts/${TINC_NAME} << %%
Subnet = ${TINC_IP}/32
%%

## Create configuration file
cat > /etc/tinc/testnet/tinc.conf << %%
Name = ${TINC_NAME}
AddressFamily = ipv4
Interface = tun7
Mode = router
PingInterval = 15
ConnectTo = tiger
ConnectTo = lion
%%

## Create 'tinc-up'
cat > /etc/tinc/testnet/tinc-up << %%
#!/bin/sh

ip link set \$INTERFACE up
ip addr add ${TINC_IP}/32 dev \$INTERFACE
ip route add 10.255.7.0/24 dev \$INTERFACE
%%

## Create 'tinc-down'
cat > /etc/tinc/testnet/tinc-down << %%
#!/bin/sh

ip route del 10.255.7.0/24 dev \$INTERFACE
ip addr del ${TINC_IP}/32 dev \$INTERFACE
ip link set \$INTERFACE down
%%

tincd -n testnet -K4096
chmod +x /etc/tinc/testnet/tinc-up
chmod +x /etc/tinc/testnet/tinc-down
