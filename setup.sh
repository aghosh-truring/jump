#!/bin/bash

function do_cmd {
	echo '     ' $*

	if [ "${TEST}" == "no" ]
	then
		$*
	fi
}

SYSCTL='/usr/sbin/sysctl'
IP='/usr/sbin/ip'
TC='/usr/sbin/tc'
IPTABLES='/usr/sbin/iptables'
BUSYBOX='/usr/bin/busybox'
IPERF='/usr/bin/iperf3'
DHRELAY='/usr/sbin/dhcrelay'
ETHTOOL='/usr/sbin/ethtool'

FILE='/simconf.json'
TEST='no'
CMD=$0

while getopts 'Lnf:' OPTION
do
	case ${OPTION} in
		L)
			NOW=`date +%Y-%m-%d-%H-%M-%S`
			LOG='/root/setup-lan.log.'${NOW}
			echo ${LOG}

			# set -x
			exec 1<&-
			exec 2<&-
			exec 1<>${LOG}
			exec 2>&1
			;;
		n)
			TEST='yes'
			;;
		f)
			FILE=${OPTARG}
			;;
	esac
done

shift $(( OPTIND - 1 ))

if [ ! -r "${FILE}" ]
then
	echo 'Bad configuration <'${FILE}'>'
	exit 1
fi

#= Make WAN =================================================================
function make_wan {
	local N
	local WAN
	local RATE
	local DELAY
	local LOSS

	local GW
	local DHCP_START
	local DHCP_STOP

	N=$1
	WAN=$2
	RATE=$3
	DELAY=$4
	LOSS=$5

	echo WAN ${N}':' ${WAN} ${RATE} ${DELAY} ${LOSS}

	GW=172.20.${N}.1
	DHCP_START=172.20.${N}.5
	DHCP_STOP=172.20.${N}.100

	cat > /tmp/W${N}.conf << %%
start           ${DHCP_START}
end             ${DHCP_STOP}
interface       W${N}
lease_file      /tmp/lease.W${N}
pidfile         /tmp/pid.W${N}
auto_time       60

option  subnet  255.255.255.0
opt     router  ${GW}
opt     dns     8.8.8.8
option  dns     8.8.4.4 
option  lease   86400
%%

	do_cmd ${IP} netns add S${N}
	do_cmd ${IP} -n S${N} addr add 127.0.0.1/8 dev lo
	do_cmd ${IP} -n S${N} link set lo up
	do_cmd ${IP} -n S${N} link add name T${N} type bridge
	do_cmd ${IP} -n S${N} link set T${N} up

	do_cmd ${IP} link add W${N} type veth peer name U${N}
	do_cmd ${IP} link set W${N} up

	do_cmd ${IP} addr add ${GW}/24 dev W${N}

	do_cmd ${BUSYBOX} udhcpd /tmp/W${N}.conf

	do_cmd ${IP} link set U${N} netns S${N}
	do_cmd ${IP} -n S${N} link set U${N} up
	do_cmd ${IP} -n S${N} link set U${N} master T${N}

	do_cmd ${IP} link set ${WAN} netns S${N}
	do_cmd ${IP} -n S${N} link set ${WAN} up
	do_cmd ${IP} -n S${N} link set ${WAN} master T${N}

	do_cmd ${TC} -n S${N} qdisc add dev ${WAN} root netem delay ${DELAY} rate ${RATE} loss ${LOSS}
	do_cmd ${TC} -n S${N} qdisc add dev U${N}  root netem delay ${DELAY} rate ${RATE} loss ${LOSS}
}

#= Make VLAN ================================================================
function make_vlan {
	local V

	V=$1

	echo VLAN ${V}

	if [ "${V}" != "0" ]
	then
		do_cmd ${IP} link add link ${MASTER} name V${V} type vlan id ${V}

		do_cmd ${IP} link add name B${V} type bridge
		do_cmd ${IP} link set V${V} master B${V}
		do_cmd ${SYSCTL} -w net.ipv4.conf.B${V}.arp_ignore=8

		do_cmd ${IP} link set B${V} up
		do_cmd ${IP} link set V${V} up
	fi
}

#= Make Router ==============================================================
function make_router {
	local R
	local ADDRESS
	local UPSTREAM
	local V

	local NS

	R=$1
	ADDRESS=$2
	UPSTREAM=$3
	V=$4

	echo ROUTER ${R}':' ${ADDRESS} ${UPSTREAM} '->' ${V}

	NS=R${R}

	do_cmd ${IP} netns add ${NS}
	do_cmd ${IP} netns exec ${NS} ${SYSCTL} -w net.ipv4.ip_forward=1

	do_cmd ${IP} link add J${R} type veth peer name K${R}
	do_cmd ${IP} link set K${R} master B${V}
	do_cmd ${IP} link set K${R} up

	do_cmd ${IP} link set J${R} netns ${NS}
	do_cmd ${IP} -n ${NS} link set J${R} up
	do_cmd ${IP} -n ${NS} address add ${ADDRESS} dev J${R}
	do_cmd ${IP} -n ${NS} route add default via ${UPSTREAM}
}

#= Make Subnet ==============================================================
function make_subnet {
	local V
	local ADDRESS
	local R

	local NS

	V=$1
	ADDRESS=$2
	R=$3

	echo SUBNET ${V} ${ADDRESS} '->' ${R}

	NS=R${R}

	do_cmd ${IP} link add name B${V} type bridge
	do_cmd ${IP} link add X${V} type veth peer name Y${V}
	do_cmd ${IP} link set X${V} master B${V}
	do_cmd ${IP} link set X${V} up
	do_cmd ${IP} link set B${V} up

	do_cmd ${IP} link set Y${V} netns ${NS}
	do_cmd ${IP} -n ${NS} link set Y${V} up
	do_cmd ${IP} -n ${NS} address add ${ADDRESS} dev Y${V}
}

#= Make Device ==============================================================
function make_device {
	local Q
	local USE

	local NS
	local MAC

	Q=$1
	USE=$2

	printf -v NS 'D%u' ${Q}
	printf -v MAC '00:02:CA:FE:00:%02X' ${Q}

	echo DEVICE ${NS} ${MAC} '->' ${USE}

	mkdir -p /etc/netns/${NS}
	cat > /etc/netns/${NS}/resolv.conf << %%
nameserver 8.8.8.8
nameserver 8.8.4.4
%%

	do_cmd ${IP} netns add ${NS}
	do_cmd ${IP} link add E${Q} type veth peer name F${Q}

	do_cmd ${IP} link set E${Q} master B${USE}
	do_cmd ${IP} link set E${Q} up

	do_cmd ${IP} link set F${Q} netns D${Q}

	do_cmd ${IP} -n ${NS} link set F${Q} addr ${MAC}
	do_cmd ${IP} -n ${NS} link set F${Q} up

	do_cmd ${IP} -n ${NS} addr add 127.0.0.1/8 dev lo
	do_cmd ${IP} -n ${NS} link set lo up
}

#===========================================================================#
#= Main Program ============================================================#
#===========================================================================#

# NOW=`date +%Y-%m-%d-%H-%M-%S`
# LOG='/root/setup-lan.log.'${NOW}

# set -x
# exec 1<&-
# exec 2<&-
# exec 1<>${LOG}
# exec 2>&1

## What is the LAN Interface?
LAN=`cat ${FILE} | jq -r -c '.lan'`
INET=`cat ${FILE} | jq -r -c '.internet'`
CURRENT_INET=`${IP} -j route show default | jq -r '.[].dev'`

N_WAN=`cat ${FILE} | jq -r -c '.wan | length'`

# Safety checks
if [ "${CURRENT_INET}" != "" ]
then
	if [ "${TEST}" == "no" -a "${INET}" != "${CURRENT_INET}" ]
	then
		echo 'ERROR: <'${INET}'> is not the Internet port, <'${CURRENT_INET}'> is'
		echo 'Aborting setup.'
		exit 3
	fi

	if [ "${TEST}" == "no" -a "${LAN}" == "${CURRENT_INET}" ]
	then
		echo 'ERROR: LAN port <'${LAN}'> clobbers the Internet port <'${CURRENT_INET}'>'
		echo 'Aborting setup.'
		exit 3
	fi

	for (( n=0; n<${N_WAN}; n++ ))
	do
		WAN=`cat ${FILE} | jq -c -r '.wan['${n}'].interface'`
		if [ "${TEST}" == "no" -a "${WAN}" == "${CURRENT_INET}" ]
		then
			echo 'ERROR: WAN port <'${WAN}'> clobbers the Internet port <'${CURRENT_INET}'>'
			echo 'Aborting setup.'
			exit 3
		fi
	done
else
	echo 'WARNING: Could not determine current Internet gateway. Safety checks skipped.'
fi
# Safety checks complete

if [ "${TEST}" == "no" -a "`id -u`" != "0" ]
then
	echo "You must be root."
	exit 2
fi

echo ${LAN} '-->' ${INET} 

# Common Part
# Enable routing
do_cmd ${SYSCTL} -w net.ipv4.ip_forward=1

# Set up NAT routing
do_cmd ${IPTABLES} -t nat -A POSTROUTING -o ${INET} -j MASQUERADE

# Set up Local Internet!
do_cmd ${IP} link add mynet type dummy
do_cmd ${IP} addr add 2.2.2.2/32 dev mynet
do_cmd ${IP} link set mynet up

# Start IPERF Server
do_cmd ${IPERF} -s -B 2.2.2.2 -D

# Set up the LAN bridge
do_cmd ${ETHTOOL} -K ${LAN} gso off gro off
MASTER='B0'
do_cmd ${IP} link add name ${MASTER} type bridge
do_cmd ${IP} link set ${LAN} master ${MASTER}
do_cmd ${SYSCTL} -w net.ipv4.conf.${MASTER}.arp_ignore=2
do_cmd ${IP} addr add 10.0.254.3/24 dev ${MASTER}
do_cmd ${IP} link set ${MASTER} up
do_cmd ${IP} link set ${LAN} up

## Build wans
for (( n=0; n<${N_WAN}; n++ ))
do
	WAN=`cat ${FILE} | jq -c -r '.wan['${n}'].interface'`
	RATE=`cat ${FILE} | jq -c -r '.wan['${n}'].rate'`
	DELAY=`cat ${FILE} | jq -c -r '.wan['${n}'].delay'`
	LOSS=`cat ${FILE} | jq -c -r '.wan['${n}'].loss'`
	make_wan ${n} ${WAN} ${RATE} ${DELAY} ${LOSS}
done

## Build non-routed vlans
for n in `cat ${FILE} | jq -r -c '.vlan[] | select(.router == null)'`
do
	V_ID=`echo ${n} | jq -r '.id'`
	make_vlan ${V_ID}
done

## Build routed vlans
R=0
for n in `cat ${FILE} | jq -r -c '.vlan[] | select(.router != null)'`
do
	V_ID=`echo ${n} | jq -r '.id'`
	ROUTER=`echo ${n} | jq -r '.router?'`
	make_vlan ${V_ID}

	## Build routers
	for m in `echo ${ROUTER} | jq -c '.[]'`
	do
		R=$((R + 1))
		ADDRESS=`echo ${m} | jq -c -r '.address'`
		UPSTREAM=`echo ${m} | jq -c -r '.upstream'`
		make_router ${R} ${ADDRESS} ${UPSTREAM} ${V_ID}

		## Build subnets
		for p in `echo ${m} | jq -c -r '.subnet[]'`
		do
			S_ID=`echo ${p} | jq -r '.id'`
			ADDRESS=`echo ${p} | jq -r '.address'`
			make_subnet ${S_ID} ${ADDRESS} ${R}
		done

		## Build argument list for relay
		DHARGS=()
		for p in `echo ${m} | jq -c -r '.subnet[]'`
		do
			S_ID=`echo ${p} | jq -r '.id'`
			DHARGS+=('-id')
			DHARGS+=(Y${S_ID})
		done

		echo ROUTER ${R}':' start DHCP relay
		## Start the relay
		do_cmd ${IP} netns exec R${R} ${DHRELAY} ${DHARGS[@]} '-iu' J${R} ${UPSTREAM}
	done
done

## Build devices
N_DEV=`cat ${FILE} | jq -r -c '.device | length'`
for (( n=0; n<${N_DEV}; n++ ))
do
	USE=`cat ${FILE} | jq -r '.device['${n}'].use'`
	make_device ${n} ${USE}
done

