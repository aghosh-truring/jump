#!/bin/bash

NS=`/usr/sbin/ip netns identify`
if [ "${NS}" != "" ]
then
	if [ ${EUID} -eq 0 ]
	then
		PS1="\[\033[01;32m\][${NS}]\[\033[0m\] ${PS1}"
	else
		PS1="\[\033[01;31m\][${NS}]\[\033[0m\] ${PS1}"
	fi
fi

alias jd='/jump/device.sh'
alias jl='/jump/link.sh'
